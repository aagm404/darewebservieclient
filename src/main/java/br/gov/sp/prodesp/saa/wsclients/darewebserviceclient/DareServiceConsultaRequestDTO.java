package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DareServiceConsultaRequestDTO", propOrder = { "numeroControleDocumentoPrincipal", "numeroControleDocumentoDetalhe" })
public class DareServiceConsultaRequestDTO {

	@XmlElement(name = "NumeroControleDocumentoPrincipal")
	protected String numeroControleDocumentoPrincipal;

	@XmlElement(name = "NumeroControleDocumentoDetalhe")
	protected String numeroControleDocumentoDetalhe;

	public String getNumeroControleDocumentoPrincipal() {
		return numeroControleDocumentoPrincipal;
	}

	public void setNumeroControleDocumentoPrincipal(String value) {
		this.numeroControleDocumentoPrincipal = value;
	}

	public String getNumeroControleDocumentoDetalhe() {
		return numeroControleDocumentoDetalhe;
	}

	public void setNumeroControleDocumentoDetalhe(String value) {
		this.numeroControleDocumentoDetalhe = value;
	}
}