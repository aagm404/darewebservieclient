package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "request" })
@XmlRootElement(name = "ConsultarDare")
public class ConsultarDare {

	@XmlElement(name = "Request")
	protected DareServiceConsultaRequestDTO request;

	public DareServiceConsultaRequestDTO getRequest() {
		return request;
	}

	public void setRequest(DareServiceConsultaRequestDTO value) {
		this.request = value;
	}
}