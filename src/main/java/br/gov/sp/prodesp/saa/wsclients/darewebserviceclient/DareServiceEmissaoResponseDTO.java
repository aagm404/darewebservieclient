package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DareServiceEmissaoResponseDTO", propOrder = { "documentoPrincipalResponse", "documentosDetalheResponse", "erros" })
@XmlSeeAlso({ DareServiceConsultaResponseDTO.class })
public class DareServiceEmissaoResponseDTO {

	@XmlElement(name = "DocumentoPrincipalResponse")
	protected DocumentoPrincipalResponse documentoPrincipalResponse;
	
	@XmlElement(name = "DocumentosDetalheResponse")
	protected ArrayOfDocumentoDetalheResponse documentosDetalheResponse;
	
	@XmlElement(name = "Erros")
	protected ArrayOfErro erros;

	public DocumentoPrincipalResponse getDocumentoPrincipalResponse() {
		return documentoPrincipalResponse;
	}

	public void setDocumentoPrincipalResponse(DocumentoPrincipalResponse value) {
		this.documentoPrincipalResponse = value;
	}

	public ArrayOfDocumentoDetalheResponse getDocumentosDetalheResponse() {
		return documentosDetalheResponse;
	}

	public void setDocumentosDetalheResponse(ArrayOfDocumentoDetalheResponse value) {
		this.documentosDetalheResponse = value;
	}

	public ArrayOfErro getErros() {
		return erros;
	}

	public void setErros(ArrayOfErro value) {
		this.erros = value;
	}
}