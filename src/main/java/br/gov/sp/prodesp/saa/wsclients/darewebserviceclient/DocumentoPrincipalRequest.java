package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoPrincipalRequest", propOrder = {
    "ipSolicitante",
    "loginSolicitante",
    "gerarPdf",
    "observacao",
    "canalEmissao",
    "orgaoEmissor",
    "sequencialEnvioEmissao"
})
public class DocumentoPrincipalRequest {

    @XmlElement(name = "IpSolicitante")
    protected String ipSolicitante;
    
    @XmlElement(name = "LoginSolicitante")
    protected String loginSolicitante;
    
    @XmlElement(name = "GerarPdf")
    protected String gerarPdf;
    
    @XmlElement(name = "Observacao")
    protected String observacao;
    
    @XmlElement(name = "CanalEmissao")
    protected int canalEmissao;
    
    @XmlElement(name = "OrgaoEmissor")
    protected int orgaoEmissor;
    
    @XmlElement(name = "SequencialEnvioEmissao")
    protected int sequencialEnvioEmissao;

	public String getIpSolicitante() {
		return ipSolicitante;
	}

	public void setIpSolicitante(String ipSolicitante) {
		this.ipSolicitante = ipSolicitante;
	}

	public String getLoginSolicitante() {
		return loginSolicitante;
	}

	public void setLoginSolicitante(String loginSolicitante) {
		this.loginSolicitante = loginSolicitante;
	}

	public String getGerarPdf() {
		return gerarPdf;
	}

	public void setGerarPdf(String gerarPdf) {
		this.gerarPdf = gerarPdf;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public int getCanalEmissao() {
		return canalEmissao;
	}

	public void setCanalEmissao(int canalEmissao) {
		this.canalEmissao = canalEmissao;
	}

	public int getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public void setOrgaoEmissor(int orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public int getSequencialEnvioEmissao() {
		return sequencialEnvioEmissao;
	}

	public void setSequencialEnvioEmissao(int sequencialEnvioEmissao) {
		this.sequencialEnvioEmissao = sequencialEnvioEmissao;
	}
}