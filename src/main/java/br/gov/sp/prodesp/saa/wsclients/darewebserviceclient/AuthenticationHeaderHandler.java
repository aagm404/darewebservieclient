package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import jakarta.xml.soap.SOAPElement;
import jakarta.xml.soap.SOAPEnvelope;
import jakarta.xml.soap.SOAPException;
import jakarta.xml.soap.SOAPHeader;
import jakarta.xml.ws.handler.MessageContext;
import jakarta.xml.ws.handler.soap.SOAPHandler;
import jakarta.xml.ws.handler.soap.SOAPMessageContext;

public class AuthenticationHeaderHandler implements SOAPHandler<SOAPMessageContext> {
	
	public final static String TAXAS_ADDR = "http://Br.Gov.Sp.Fazenda.Taxas";
    public final static String DARESERVICE_NAMESPACE = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService";
    
    private AuthenticationSoapHeaderEntity auth;
    private QName authQname, loginQname, senhaQname;
    
    public AuthenticationHeaderHandler(AuthenticationSoapHeaderEntity auth, String address) {
        this.auth = auth;
        this.authQname = new QName(address, "AuthenticationSoapHeaderEntity");
        this.loginQname = new QName(address, "Login");
        this.senhaQname = new QName(address, "Senha");
    }

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		 
        if (outboundProperty.booleanValue()) {
            
            try {  
                SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
                
				if (envelope.getHeader() != null) {
					envelope.getHeader().detachNode();
				}
                
                SOAPHeader header = envelope.addHeader();                
                
                SOAPElement authSoap = header.addChildElement(authQname);
                
                SOAPElement loginSoap = authSoap.addChildElement(loginQname);
                loginSoap.addTextNode(auth.getLogin());
                
                SOAPElement senhaSoap = authSoap.addChildElement(senhaQname);
                senhaSoap.addTextNode(auth.getSenha());
                
            } catch (SOAPException e) {                
                Logger.getLogger(AuthenticationHeaderHandler.class.getName()).log(Level.SEVERE, null, e);
            }
        } 
 
        return outboundProperty;
		
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {		
		return false;
	}

	@Override
	public void close(MessageContext context) {
		
	}

	@Override
	public Set<QName> getHeaders() {		
		return null;
	}
}