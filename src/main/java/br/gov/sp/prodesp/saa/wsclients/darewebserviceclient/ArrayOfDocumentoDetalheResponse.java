package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDocumentoDetalheResponse", propOrder = { "documentoDetalheResponse" })
public class ArrayOfDocumentoDetalheResponse {

	@XmlElement(name = "DocumentoDetalheResponse", nillable = true)
	protected List<DocumentoDetalheResponse> documentoDetalheResponse;

	public List<DocumentoDetalheResponse> getDocumentoDetalheResponse() {
		if (documentoDetalheResponse == null) {
			documentoDetalheResponse = new ArrayList<DocumentoDetalheResponse>();
		}
		return this.documentoDetalheResponse;
	}
}