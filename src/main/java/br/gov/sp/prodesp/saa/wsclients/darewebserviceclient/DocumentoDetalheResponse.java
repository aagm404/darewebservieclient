package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.math.BigDecimal;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoDetalheResponse", propOrder = {
    "codigoDocumentoDetalhe",
    "numeroControleDocumentoDetalhe",
    "sequencialRegistroDetalhe",
    "indicadorImpressaoDetalhe",
    "indicadorRegistroDetalhe",
    "codigoReceita",
    "tipoServico",
    "descricaoTipoServico",
    "dataVencimento",
    "linha04",
    "linha04DescricaoInformacao",
    "linha05",
    "linha05DescricaoInformacao",
    "linha06",
    "linha06DescricaoInformacao",
    "linha08",
    "linha08DescricaoInformacao",
    "referencia",
    "valorReceita",
    "valorJurosMora",
    "valorMultaMora",
    "valorAcrescimoFinanceiro",
    "valorHonorariosAdvocaticios",
    "valorTotalDetalhe",
    "observacao",
    "nomeContribuinte",
    "endereco",
    "cidade",
    "estado",
    "tipoTelefone",
    "telefone",
    "placa",
    "cnae",
    "reservado01",
    "reservado02",
    "reservado03",
    "reservado04",
    "reservado05",
    "reservado06",
    "reservado07",
    "reservado08",
    "reservado09",
    "reservado10"
})
public class DocumentoDetalheResponse {

    @XmlElement(name = "CodigoDocumentoDetalhe")
    protected long codigoDocumentoDetalhe;
    
    @XmlElement(name = "NumeroControleDocumentoDetalhe")
    protected String numeroControleDocumentoDetalhe;
    
    @XmlElement(name = "SequencialRegistroDetalhe")
    protected String sequencialRegistroDetalhe;
    
    @XmlElement(name = "IndicadorImpressaoDetalhe")
    protected String indicadorImpressaoDetalhe;
    
    @XmlElement(name = "IndicadorRegistroDetalhe")
    protected int indicadorRegistroDetalhe;
    
    @XmlElement(name = "CodigoReceita")
    protected int codigoReceita;
    
    @XmlElement(name = "TipoServico")
    protected int tipoServico;
    
    @XmlElement(name = "DescricaoTipoServico")
    protected String descricaoTipoServico;
    
    @XmlElement(name = "DataVencimento")
    protected String dataVencimento;
    
    @XmlElement(name = "Linha04")
    protected String linha04;
    
    @XmlElement(name = "Linha04DescricaoInformacao")
    protected String linha04DescricaoInformacao;
    
    @XmlElement(name = "Linha05")
    protected String linha05;
    
    @XmlElement(name = "Linha05DescricaoInformacao")
    protected String linha05DescricaoInformacao;
    
    @XmlElement(name = "Linha06")
    protected String linha06;
    
    @XmlElement(name = "Linha06DescricaoInformacao")
    protected String linha06DescricaoInformacao;
    
    @XmlElement(name = "Linha08")
    protected String linha08;
    
    @XmlElement(name = "Linha08DescricaoInformacao")
    protected String linha08DescricaoInformacao;
    
    @XmlElement(name = "Referencia")
    protected String referencia;
    
    @XmlElement(name = "ValorReceita", required = true)
    protected BigDecimal valorReceita;
    
    @XmlElement(name = "ValorJurosMora", required = true)
    protected BigDecimal valorJurosMora;
    
    @XmlElement(name = "ValorMultaMora", required = true)
    protected BigDecimal valorMultaMora;
    
    @XmlElement(name = "ValorAcrescimoFinanceiro", required = true)
    protected BigDecimal valorAcrescimoFinanceiro;
    
    @XmlElement(name = "ValorHonorariosAdvocaticios", required = true)
    protected BigDecimal valorHonorariosAdvocaticios;
    
    @XmlElement(name = "ValorTotalDetalhe", required = true)
    protected BigDecimal valorTotalDetalhe;
    
    @XmlElement(name = "Observacao")
    protected String observacao;
    
    @XmlElement(name = "NomeContribuinte")
    protected String nomeContribuinte;
    
    @XmlElement(name = "Endereco")
    protected String endereco;
    
    @XmlElement(name = "Cidade")
    protected String cidade;
    
    @XmlElement(name = "Estado")
    protected String estado;
    
    @XmlElement(name = "TipoTelefone")
    protected String tipoTelefone;
    
    @XmlElement(name = "Telefone")
    protected String telefone;
    
    @XmlElement(name = "Placa")
    protected String placa;
    
    @XmlElement(name = "CNAE")
    protected String cnae;
    
    @XmlElement(name = "Reservado01")
    protected String reservado01;
    
    @XmlElement(name = "Reservado02")
    protected String reservado02;
    
    @XmlElement(name = "Reservado03")
    protected String reservado03;
    
    @XmlElement(name = "Reservado04")
    protected String reservado04;
    
    @XmlElement(name = "Reservado05")
    protected String reservado05;
    
    @XmlElement(name = "Reservado06")
    protected String reservado06;
    
    @XmlElement(name = "Reservado07")
    protected String reservado07;
    
    @XmlElement(name = "Reservado08")
    protected String reservado08;
    
    @XmlElement(name = "Reservado09")
    protected String reservado09;
    
    @XmlElement(name = "Reservado10")
    protected String reservado10;

	public long getCodigoDocumentoDetalhe() {
		return codigoDocumentoDetalhe;
	}

	public void setCodigoDocumentoDetalhe(long codigoDocumentoDetalhe) {
		this.codigoDocumentoDetalhe = codigoDocumentoDetalhe;
	}

	public String getNumeroControleDocumentoDetalhe() {
		return numeroControleDocumentoDetalhe;
	}

	public void setNumeroControleDocumentoDetalhe(String numeroControleDocumentoDetalhe) {
		this.numeroControleDocumentoDetalhe = numeroControleDocumentoDetalhe;
	}

	public String getSequencialRegistroDetalhe() {
		return sequencialRegistroDetalhe;
	}

	public void setSequencialRegistroDetalhe(String sequencialRegistroDetalhe) {
		this.sequencialRegistroDetalhe = sequencialRegistroDetalhe;
	}

	public String getIndicadorImpressaoDetalhe() {
		return indicadorImpressaoDetalhe;
	}

	public void setIndicadorImpressaoDetalhe(String indicadorImpressaoDetalhe) {
		this.indicadorImpressaoDetalhe = indicadorImpressaoDetalhe;
	}

	public int getIndicadorRegistroDetalhe() {
		return indicadorRegistroDetalhe;
	}

	public void setIndicadorRegistroDetalhe(int indicadorRegistroDetalhe) {
		this.indicadorRegistroDetalhe = indicadorRegistroDetalhe;
	}

	public int getCodigoReceita() {
		return codigoReceita;
	}

	public void setCodigoReceita(int codigoReceita) {
		this.codigoReceita = codigoReceita;
	}

	public int getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(int tipoServico) {
		this.tipoServico = tipoServico;
	}

	public String getDescricaoTipoServico() {
		return descricaoTipoServico;
	}

	public void setDescricaoTipoServico(String descricaoTipoServico) {
		this.descricaoTipoServico = descricaoTipoServico;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getLinha04() {
		return linha04;
	}

	public void setLinha04(String linha04) {
		this.linha04 = linha04;
	}

	public String getLinha04DescricaoInformacao() {
		return linha04DescricaoInformacao;
	}

	public void setLinha04DescricaoInformacao(String linha04DescricaoInformacao) {
		this.linha04DescricaoInformacao = linha04DescricaoInformacao;
	}

	public String getLinha05() {
		return linha05;
	}

	public void setLinha05(String linha05) {
		this.linha05 = linha05;
	}

	public String getLinha05DescricaoInformacao() {
		return linha05DescricaoInformacao;
	}

	public void setLinha05DescricaoInformacao(String linha05DescricaoInformacao) {
		this.linha05DescricaoInformacao = linha05DescricaoInformacao;
	}

	public String getLinha06() {
		return linha06;
	}

	public void setLinha06(String linha06) {
		this.linha06 = linha06;
	}

	public String getLinha06DescricaoInformacao() {
		return linha06DescricaoInformacao;
	}

	public void setLinha06DescricaoInformacao(String linha06DescricaoInformacao) {
		this.linha06DescricaoInformacao = linha06DescricaoInformacao;
	}

	public String getLinha08() {
		return linha08;
	}

	public void setLinha08(String linha08) {
		this.linha08 = linha08;
	}

	public String getLinha08DescricaoInformacao() {
		return linha08DescricaoInformacao;
	}

	public void setLinha08DescricaoInformacao(String linha08DescricaoInformacao) {
		this.linha08DescricaoInformacao = linha08DescricaoInformacao;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public BigDecimal getValorReceita() {
		return valorReceita;
	}

	public void setValorReceita(BigDecimal valorReceita) {
		this.valorReceita = valorReceita;
	}

	public BigDecimal getValorJurosMora() {
		return valorJurosMora;
	}

	public void setValorJurosMora(BigDecimal valorJurosMora) {
		this.valorJurosMora = valorJurosMora;
	}

	public BigDecimal getValorMultaMora() {
		return valorMultaMora;
	}

	public void setValorMultaMora(BigDecimal valorMultaMora) {
		this.valorMultaMora = valorMultaMora;
	}

	public BigDecimal getValorAcrescimoFinanceiro() {
		return valorAcrescimoFinanceiro;
	}

	public void setValorAcrescimoFinanceiro(BigDecimal valorAcrescimoFinanceiro) {
		this.valorAcrescimoFinanceiro = valorAcrescimoFinanceiro;
	}

	public BigDecimal getValorHonorariosAdvocaticios() {
		return valorHonorariosAdvocaticios;
	}

	public void setValorHonorariosAdvocaticios(BigDecimal valorHonorariosAdvocaticios) {
		this.valorHonorariosAdvocaticios = valorHonorariosAdvocaticios;
	}

	public BigDecimal getValorTotalDetalhe() {
		return valorTotalDetalhe;
	}

	public void setValorTotalDetalhe(BigDecimal valorTotalDetalhe) {
		this.valorTotalDetalhe = valorTotalDetalhe;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getNomeContribuinte() {
		return nomeContribuinte;
	}

	public void setNomeContribuinte(String nomeContribuinte) {
		this.nomeContribuinte = nomeContribuinte;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(String tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getCnae() {
		return cnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

	public String getReservado01() {
		return reservado01;
	}

	public void setReservado01(String reservado01) {
		this.reservado01 = reservado01;
	}

	public String getReservado02() {
		return reservado02;
	}

	public void setReservado02(String reservado02) {
		this.reservado02 = reservado02;
	}

	public String getReservado03() {
		return reservado03;
	}

	public void setReservado03(String reservado03) {
		this.reservado03 = reservado03;
	}

	public String getReservado04() {
		return reservado04;
	}

	public void setReservado04(String reservado04) {
		this.reservado04 = reservado04;
	}

	public String getReservado05() {
		return reservado05;
	}

	public void setReservado05(String reservado05) {
		this.reservado05 = reservado05;
	}

	public String getReservado06() {
		return reservado06;
	}

	public void setReservado06(String reservado06) {
		this.reservado06 = reservado06;
	}

	public String getReservado07() {
		return reservado07;
	}

	public void setReservado07(String reservado07) {
		this.reservado07 = reservado07;
	}

	public String getReservado08() {
		return reservado08;
	}

	public void setReservado08(String reservado08) {
		this.reservado08 = reservado08;
	}

	public String getReservado09() {
		return reservado09;
	}

	public void setReservado09(String reservado09) {
		this.reservado09 = reservado09;
	}

	public String getReservado10() {
		return reservado10;
	}

	public void setReservado10(String reservado10) {
		this.reservado10 = reservado10;
	}
}