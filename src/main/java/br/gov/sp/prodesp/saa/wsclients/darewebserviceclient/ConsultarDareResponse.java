package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "consultarDareResult" })
@XmlRootElement(name = "ConsultarDareResponse")
public class ConsultarDareResponse {

	@XmlElement(name = "ConsultarDareResult")
	protected DareServiceConsultaResponseDTO consultarDareResult;

	public DareServiceConsultaResponseDTO getConsultarDareResult() {
		return consultarDareResult;
	}

	public void setConsultarDareResult(DareServiceConsultaResponseDTO value) {
		this.consultarDareResult = value;
	}
}