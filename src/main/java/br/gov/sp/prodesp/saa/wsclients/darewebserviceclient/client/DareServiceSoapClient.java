package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.client;

import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.AuthenticationHeaderHandler;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.AuthenticationHeaderHandlerResolver;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.AuthenticationSoapHeaderEntity;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareProperty;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareService;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareServiceConsultaRequestDTO;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareServiceConsultaResponseDTO;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareServiceEmissaoRequestDTO;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareServiceEmissaoResponseDTO;
import br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.DareServiceSoap;

public class DareServiceSoapClient {

	private String login = DareProperty.getLogin();
	private String senha = DareProperty.getSenha();
	private String codigoOrgao = DareProperty.getCodigoOrgao();
	private String identificadorEmissor = DareProperty.getIdentificadorEmissor();
	private String identificadorRegistro = DareProperty.getIdentificadorRegistro();
	private String ipUsuario = "10.10.10.10";
	private Integer indicadorRegistroDetalhe = 1;
	private String tipoTelefoneNacional = "N";

	public DareServiceConsultaResponseDTO consultarDare(DareServiceConsultaRequestDTO requestConsulta) {
		DareServiceSoap soapClient = getSoapClient();
		DareServiceConsultaResponseDTO responseConsulta = soapClient.consultarDare(requestConsulta);

		return responseConsulta;
	}

	public DareServiceEmissaoResponseDTO emitirDare(DareServiceEmissaoRequestDTO requestEmissao) {
		DareServiceSoap soapClient = getSoapClient();
		DareServiceEmissaoResponseDTO responseEmissao = soapClient.emitirDare(requestEmissao);

		return responseEmissao;
	}

	private DareServiceSoap getSoapClient() {
		AuthenticationHeaderHandlerResolver resolver = autenticar();

		DareService dareService = new DareService();
		dareService.setHandlerResolver(resolver);

		DareServiceSoap client = dareService.getDareServiceSoap();

		return client;
	}

	private AuthenticationHeaderHandlerResolver autenticar() {
		AuthenticationSoapHeaderEntity auth = new AuthenticationSoapHeaderEntity();
		auth.setLogin(login);
		auth.setSenha(senha);

		AuthenticationHeaderHandlerResolver resolver = new AuthenticationHeaderHandlerResolver(auth,
				AuthenticationHeaderHandler.DARESERVICE_NAMESPACE);

		return resolver;
	}

	public String getCodigoOrgao() {
		return codigoOrgao;
	}

	public String getIdentificadorEmissor() {
		return identificadorEmissor;
	}

	public String getIdentificadorRegistro() {
		return identificadorRegistro;
	}

	public String getIpUsuario() {
		return ipUsuario;
	}

	public Integer getIndicadorRegistroDetalhe() {
		return indicadorRegistroDetalhe;
	}

	public String getTipoTelefoneNacional() {
		return tipoTelefoneNacional;
	}
}