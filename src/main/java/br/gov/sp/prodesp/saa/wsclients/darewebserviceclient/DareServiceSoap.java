package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.ws.RequestWrapper;
import jakarta.xml.ws.ResponseWrapper;

@WebService(name = "DareServiceSoap", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService")
@XmlSeeAlso({ ObjectFactory.class })
public interface DareServiceSoap {


    @WebMethod(operationName = "EmitirDare", action = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService/EmitirDare")
    @WebResult(name = "EmitirDareResult", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService")
    @RequestWrapper(localName = "EmitirDare", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", className = "br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.EmitirDare")
    @ResponseWrapper(localName = "EmitirDareResponse", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", className = "br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.EmitirDareResponse")
    public DareServiceEmissaoResponseDTO emitirDare(
        @WebParam(name = "Request", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService")
        DareServiceEmissaoRequestDTO request);

    @WebMethod(operationName = "ConsultarDare", action = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService/ConsultarDare")
    @WebResult(name = "ConsultarDareResult", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService")
    @RequestWrapper(localName = "ConsultarDare", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", className = "br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.ConsultarDare")
    @ResponseWrapper(localName = "ConsultarDareResponse", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", className = "br.gov.sp.prodesp.saa.wsclients.darewebserviceclient.ConsultarDareResponse")
    public DareServiceConsultaResponseDTO consultarDare(
        @WebParam(name = "Request", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService")
        DareServiceConsultaRequestDTO request);
}