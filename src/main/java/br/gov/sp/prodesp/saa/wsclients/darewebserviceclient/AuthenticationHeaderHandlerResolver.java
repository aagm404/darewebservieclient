package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.ws.handler.Handler;
import jakarta.xml.ws.handler.HandlerResolver;
import jakarta.xml.ws.handler.PortInfo;

public class AuthenticationHeaderHandlerResolver implements HandlerResolver {
	
	private AuthenticationSoapHeaderEntity auth;
    private String address;
    
    public AuthenticationHeaderHandlerResolver(AuthenticationSoapHeaderEntity auth, String address) {
        this.auth = auth;
        this.address = address;
    }

	@SuppressWarnings("rawtypes")
	@Override
	public List<Handler> getHandlerChain(PortInfo portInfo) {
		 List<Handler> handlerChain = new ArrayList<Handler>(); 
	     AuthenticationHeaderHandler hh = new AuthenticationHeaderHandler(auth, address); 
	     handlerChain.add(hh);
	 
	     return handlerChain;
	}
}