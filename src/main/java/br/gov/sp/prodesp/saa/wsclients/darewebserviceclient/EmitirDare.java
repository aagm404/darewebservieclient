package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "request" })
@XmlRootElement(name = "EmitirDare")
public class EmitirDare {

	@XmlElement(name = "Request")
	protected DareServiceEmissaoRequestDTO request;

	public DareServiceEmissaoRequestDTO getRequest() {
		return request;
	}

	public void setRequest(DareServiceEmissaoRequestDTO value) {
		this.request = value;
	}
}