package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import jakarta.xml.ws.Service;
import jakarta.xml.ws.WebEndpoint;
import jakarta.xml.ws.WebServiceClient;
import jakarta.xml.ws.WebServiceException;
import jakarta.xml.ws.WebServiceFeature;

@WebServiceClient(name = "DareService", targetNamespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService")
public class DareService extends Service {

	private final static URL DARESERVICE_WSDL_LOCATION;
	private final static WebServiceException DARESERVICE_EXCEPTION;
	private final static QName DARESERVICE_QNAME = new QName("http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", "DareService");

	private final static Logger logger = Logger.getLogger(DareService.class.getName());
	private final static String DARE_URL = "https://" + DareProperty.getUrl() + "/Pagamentos/WS/DareService.asmx?WSDL";

	static {

		URL url = null;
		WebServiceException e = null;

		try {

			URL baseUrl;
			baseUrl = DareService.class.getResource(".");
			url = new URL(baseUrl, DARE_URL);
			logger.info("URL: " + DARE_URL);

		} catch (MalformedURLException ex) {
			logger.warning("Failed to create URL for the wsdl Location: " + DARE_URL + ", retrying as a local file");
			logger.warning(ex.getMessage());
			e = new WebServiceException(ex);
		}

		DARESERVICE_WSDL_LOCATION = url;
		DARESERVICE_EXCEPTION = e;
	}

	public DareService() {
		super(__getWsdlLocation(), DARESERVICE_QNAME);
	}

	public DareService(WebServiceFeature... features) {
		super(__getWsdlLocation(), DARESERVICE_QNAME, features);
	}

	public DareService(URL wsdlLocation) {
		super(wsdlLocation, DARESERVICE_QNAME);
	}

	public DareService(URL wsdlLocation, WebServiceFeature... features) {
		super(wsdlLocation, DARESERVICE_QNAME, features);
	}

	public DareService(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public DareService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
		super(wsdlLocation, serviceName, features);
	}

	@WebEndpoint(name = "DareServiceSoap")
	public DareServiceSoap getDareServiceSoap() {
		return super.getPort(
				new QName("http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", "DareServiceSoap"),
				DareServiceSoap.class);
	}

	@WebEndpoint(name = "DareServiceSoap")
	public DareServiceSoap getDareServiceSoap(WebServiceFeature... features) {
		return super.getPort(
				new QName("http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", "DareServiceSoap"),
				DareServiceSoap.class, features);
	}

	@WebEndpoint(name = "DareServiceSoap12")
	public DareServiceSoap getDareServiceSoap12() {
		return super.getPort(
				new QName("http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", "DareServiceSoap12"),
				DareServiceSoap.class);
	}

	@WebEndpoint(name = "DareServiceSoap12")
	public DareServiceSoap getDareServiceSoap12(WebServiceFeature... features) {
		return super.getPort(
				new QName("http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", "DareServiceSoap12"),
				DareServiceSoap.class, features);
	}

	private static URL __getWsdlLocation() {
		if (DARESERVICE_EXCEPTION != null) {
			throw DARESERVICE_EXCEPTION;
		}
		return DARESERVICE_WSDL_LOCATION;
	}
}