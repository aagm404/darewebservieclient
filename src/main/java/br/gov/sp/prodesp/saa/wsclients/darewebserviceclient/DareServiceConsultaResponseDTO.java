package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DareServiceConsultaResponseDTO")
@XmlRootElement
public class DareServiceConsultaResponseDTO extends DareServiceEmissaoResponseDTO {

}