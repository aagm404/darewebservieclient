package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.math.BigDecimal;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoPrincipalResponse", propOrder = {
    "codigoDocumentoPrincipal",
    "sequencialDocumentoPrincipal",
    "codigoBarra",
    "numeroControleDarePrincipal",
    "ipSolicitante",
    "sequencialEnvioEmissao",
    "cpfcnpjBase",
    "nomeContribuinte",
    "endereco",
    "cidade",
    "estado",
    "tipoTelefone",
    "telefone",
    "valorTotalReceita",
    "valorTotalJurosMora",
    "valorTotalMulta",
    "valorTotalAcrescimoFinanceiro",
    "valorTotalHonorarioAdvocaticio",
    "valorTotal",
    "documentoPago",
    "quantidadeDocumentosDetalhe",
    "dataVencimento",
    "dataEmissao",
    "gerarPDF",
    "canalEmissao",
    "orgaoEmissor",
    "observacao",
    "dareImpressoPDF"
})
public class DocumentoPrincipalResponse {

    @XmlElement(name = "CodigoDocumentoPrincipal")
    protected long codigoDocumentoPrincipal;

    @XmlElement(name = "SequencialDocumentoPrincipal")
    protected long sequencialDocumentoPrincipal;

    @XmlElement(name = "CodigoBarra")
    protected String codigoBarra;

    @XmlElement(name = "NumeroControleDarePrincipal")
    protected long numeroControleDarePrincipal;

    @XmlElement(name = "IpSolicitante")
    protected String ipSolicitante;

    @XmlElement(name = "SequencialEnvioEmissao")
    protected String sequencialEnvioEmissao;

    @XmlElement(name = "CPFCNPJBase")
    protected String cpfcnpjBase;

    @XmlElement(name = "NomeContribuinte")
    protected String nomeContribuinte;

    @XmlElement(name = "Endereco")
    protected String endereco;

    @XmlElement(name = "Cidade")
    protected String cidade;

    @XmlElement(name = "Estado")
    protected String estado;

    @XmlElement(name = "TipoTelefone")
    protected String tipoTelefone;

    @XmlElement(name = "Telefone")
    protected String telefone;

    @XmlElement(name = "ValorTotalReceita", required = true)
    protected BigDecimal valorTotalReceita;

    @XmlElement(name = "ValorTotalJurosMora", required = true)
    protected BigDecimal valorTotalJurosMora;

    @XmlElement(name = "ValorTotalMulta", required = true)
    protected BigDecimal valorTotalMulta;

    @XmlElement(name = "ValorTotalAcrescimoFinanceiro", required = true)
    protected BigDecimal valorTotalAcrescimoFinanceiro;

    @XmlElement(name = "ValorTotalHonorarioAdvocaticio", required = true)
    protected BigDecimal valorTotalHonorarioAdvocaticio;

    @XmlElement(name = "ValorTotal", required = true)
    protected BigDecimal valorTotal;

    @XmlElement(name = "DocumentoPago")
    protected String documentoPago;

    @XmlElement(name = "QuantidadeDocumentosDetalhe")
    protected int quantidadeDocumentosDetalhe;

    @XmlElement(name = "DataVencimento")
    protected String dataVencimento;

    @XmlElement(name = "DataEmissao")
    protected String dataEmissao;

    @XmlElement(name = "GerarPDF")
    protected String gerarPDF;

    @XmlElement(name = "CanalEmissao")
    protected String canalEmissao;

    @XmlElement(name = "OrgaoEmissor")
    protected String orgaoEmissor;

    @XmlElement(name = "Observacao")
    protected String observacao;

    @XmlElement(name = "DareImpressoPDF")
    protected String dareImpressoPDF;

	public long getCodigoDocumentoPrincipal() {
		return codigoDocumentoPrincipal;
	}

	public void setCodigoDocumentoPrincipal(long codigoDocumentoPrincipal) {
		this.codigoDocumentoPrincipal = codigoDocumentoPrincipal;
	}

	public long getSequencialDocumentoPrincipal() {
		return sequencialDocumentoPrincipal;
	}

	public void setSequencialDocumentoPrincipal(long sequencialDocumentoPrincipal) {
		this.sequencialDocumentoPrincipal = sequencialDocumentoPrincipal;
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public long getNumeroControleDarePrincipal() {
		return numeroControleDarePrincipal;
	}

	public void setNumeroControleDarePrincipal(long numeroControleDarePrincipal) {
		this.numeroControleDarePrincipal = numeroControleDarePrincipal;
	}

	public String getIpSolicitante() {
		return ipSolicitante;
	}

	public void setIpSolicitante(String ipSolicitante) {
		this.ipSolicitante = ipSolicitante;
	}

	public String getSequencialEnvioEmissao() {
		return sequencialEnvioEmissao;
	}

	public void setSequencialEnvioEmissao(String sequencialEnvioEmissao) {
		this.sequencialEnvioEmissao = sequencialEnvioEmissao;
	}

	public String getCpfcnpjBase() {
		return cpfcnpjBase;
	}

	public void setCpfcnpjBase(String cpfcnpjBase) {
		this.cpfcnpjBase = cpfcnpjBase;
	}

	public String getNomeContribuinte() {
		return nomeContribuinte;
	}

	public void setNomeContribuinte(String nomeContribuinte) {
		this.nomeContribuinte = nomeContribuinte;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTipoTelefone() {
		return tipoTelefone;
	}

	public void setTipoTelefone(String tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public BigDecimal getValorTotalReceita() {
		return valorTotalReceita;
	}

	public void setValorTotalReceita(BigDecimal valorTotalReceita) {
		this.valorTotalReceita = valorTotalReceita;
	}

	public BigDecimal getValorTotalJurosMora() {
		return valorTotalJurosMora;
	}

	public void setValorTotalJurosMora(BigDecimal valorTotalJurosMora) {
		this.valorTotalJurosMora = valorTotalJurosMora;
	}

	public BigDecimal getValorTotalMulta() {
		return valorTotalMulta;
	}

	public void setValorTotalMulta(BigDecimal valorTotalMulta) {
		this.valorTotalMulta = valorTotalMulta;
	}

	public BigDecimal getValorTotalAcrescimoFinanceiro() {
		return valorTotalAcrescimoFinanceiro;
	}

	public void setValorTotalAcrescimoFinanceiro(BigDecimal valorTotalAcrescimoFinanceiro) {
		this.valorTotalAcrescimoFinanceiro = valorTotalAcrescimoFinanceiro;
	}

	public BigDecimal getValorTotalHonorarioAdvocaticio() {
		return valorTotalHonorarioAdvocaticio;
	}

	public void setValorTotalHonorarioAdvocaticio(BigDecimal valorTotalHonorarioAdvocaticio) {
		this.valorTotalHonorarioAdvocaticio = valorTotalHonorarioAdvocaticio;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getDocumentoPago() {
		return documentoPago;
	}

	public void setDocumentoPago(String documentoPago) {
		this.documentoPago = documentoPago;
	}

	public int getQuantidadeDocumentosDetalhe() {
		return quantidadeDocumentosDetalhe;
	}

	public void setQuantidadeDocumentosDetalhe(int quantidadeDocumentosDetalhe) {
		this.quantidadeDocumentosDetalhe = quantidadeDocumentosDetalhe;
	}

	public String getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(String dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getGerarPDF() {
		return gerarPDF;
	}

	public void setGerarPDF(String gerarPDF) {
		this.gerarPDF = gerarPDF;
	}

	public String getCanalEmissao() {
		return canalEmissao;
	}

	public void setCanalEmissao(String canalEmissao) {
		this.canalEmissao = canalEmissao;
	}

	public String getOrgaoEmissor() {
		return orgaoEmissor;
	}

	public void setOrgaoEmissor(String orgaoEmissor) {
		this.orgaoEmissor = orgaoEmissor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getDareImpressoPDF() {
		return dareImpressoPDF;
	}

	public void setDareImpressoPDF(String dareImpressoPDF) {
		this.dareImpressoPDF = dareImpressoPDF;
	}
}