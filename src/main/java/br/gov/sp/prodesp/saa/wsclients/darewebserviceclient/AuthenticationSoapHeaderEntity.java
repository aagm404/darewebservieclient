package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAnyAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthenticationSoapHeaderEntity", propOrder = { "login", "senha" })
public class AuthenticationSoapHeaderEntity {

	@XmlElement(name = "Login")
	protected String login;
	
	@XmlElement(name = "Senha")
	protected String senha;
	
	@XmlAnyAttribute
	private Map<QName, String> otherAttributes = new HashMap<QName, String>();

	public String getLogin() {
		return login;
	}

	public void setLogin(String value) {
		this.login = value;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String value) {
		this.senha = value;
	}

	public Map<QName, String> getOtherAttributes() {
		return otherAttributes;
	}
}