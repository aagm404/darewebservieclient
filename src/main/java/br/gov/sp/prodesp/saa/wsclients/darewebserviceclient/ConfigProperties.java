package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Permite a recuperação de valores de configuração cadastrados em um arquivo de propriedades.
 * <p>
 * Todos os valores são opcionais. Se uma propriedade não está cadastrada ou até mesmo se o arquivo não existe, o método
 * <code>readXXX()</code> retorna um valor <i>default </i>.
 * <p>
 * Na prática, ninguém quer que a aplicação caia se uma propriedade de configuração não foi encontrada. Ao invés disso o
 * melhor é informar o valor que foi assumido e seguir em frente.
 * <p>
 * Dentro do arquivo, as propriedades deverão estar cadastradas como no exemplo abaixo:
 * <p>
 * <code><pre>
 * localPadrao = São Paulo  
 * <br>
 * valorMaximo = 50
 * <br>
 * pagamentoFlag = true
 * <p>
 * </pre></code> Exemplo de utilização:
 * <p>
 * <code><pre>
 *       
 *        public static ConfigProperties props = 
 *              new ConfigProperties(&quot;application&quot;);
 *        
 *        ...
 *        
 *        String local = props.readString(&quot;localPadrao&quot;);
 *        int valor = props.readInt(&quot;valorMaximo&quot;);
 *        boolean = props.readBoolean(&quot;pagamentoFlag&quot;);
 *   
 *  
 * </pre></code> Uma vez recuperada a propriedade através de um dos métodos <code>readXXX()</code> ela é mantida em
 * cache para prover performance.
 * <p>
 * Uma aplicação pode ter muitos arquivos de propriedades, cada qual com seu conjunto de propriedades. Cada arquivo de
 * propriedade deve ser representado por uma instância da classe <code>ConfigProperties</code>.
 * <p>
 * As instâncias de <code>ConfigProperties</code> podem ser acessadas simultaneamente por mais de uma <i>thread </i>.
 * <p>
 * Também é possível associar um conjunto de arquivos de propriedades a uma mesma instância:
 * <p>
 * <code><pre>
 * public static final String[] PROP_FILES = { &quot;prop1&quot;, &quot;prop2&quot;, &quot;prop3&quot; };
 * public static ConfigProperties props = new ConfigProperties(PROP_FILES);
 * 
 * </pre></code> Neste caso a propriedade é procurada dentro dos arquivos na ordem em que eles foram declarados. Se ela
 * não estiver cadastrada em nenhum deles é retornado o valor <i>default </i>.
 * <p>
 * O arquivo de propriedades deve estar localizado em algum lugar sob o classpath. Pode estar também dentro de um
 * arquivo JAR. Neste caso o próprio arquivo JAR deve estar no classpath.
 * <p>
 * Este componente foi baseado no <code>SummaConfigSingleton</code> cujo fonte foi cedido por Summa Technologies.
 * <p>
 * 
 * @author Marco Minici
 */
public final class ConfigProperties {
	
	private final static Logger logger = Logger.getLogger(ConfigProperties.class.getName());

    private Properties[] properties = null;

    private Map<String, String> cache = Collections.synchronizedMap(new HashMap<String, String>());

    /**
     * Lê as propriedades a partir do arquivo "<filename>.properties".
     * <p>
     * 
     * @param filename
     *            o nome do arquivo de propriedades.
     */
    public ConfigProperties(String filename) {
        properties = new Properties[1];
        properties[0] = loadProperties(filename);
    }

    /**
     * Lê as propriedades a partir dos arquivos "<filename>.properties", começaando pelo mais específico
     * (i=0) para o mais genérico (i=n-1).
     * <p>
     * 
     * @param filenames
     *            array com os nomes dos arquivos de propriedades.
     */
    public ConfigProperties(String[] filenames) {
        int size = (filenames != null ? filenames.length : 0);
        properties = new Properties[size];
        for (int i = 0; i < size; i++) {
            properties[i] = loadProperties(filenames[i]);
        }
    }

    /**
     * Retorna o valor de uma propriedade como um booleano.
     * <p>
     * Se a propriedade não existir ou não puder ser convertida para um tipo <code>boolean</code>, retorna
     * <code>false</code>.
     * <p>
     * 
     * @param key
     *            nome da propriedade.
     * @return o valor da propriedade.
     */
    public boolean readBoolean(String propertyName) {
        return Boolean.valueOf(readString(propertyName)).booleanValue();
    }

    /**
     * Retorna o valor de uma propriedade como um número inteiro.
     * <p>
     * Se a propriedade não existir ou não puder ser convertida para um tipo <code>int</code> (inteiro), retorna
     * <code>0</code> (zero).
     * <p>
     * 
     * @param key
     *            nome da propriedade.
     * @return o valor da propriedade.
     */
    public int readInt(String propertyName) {
        try {
            return Integer.parseInt(readString(propertyName));
        }
        catch (Exception e) {
            return 0;
        }
    }

    /**
     * Retorna o valor de uma propriedade como uma String.
     * <p>
     * Se o parâmetro não existir retorna uma String vazia.
     * <p>
     * 
     * @param key
     *            nome da propriedade.
     * @return o valor da propriedade.
     */
    public String readString(String propertyName) {
        String value = (String) cache.get(propertyName);
        if (value == null) {
            value = loadString(propertyName);
            cache.put(propertyName, value);
        }
        return value;
    }

    /**
     * Confirma a existência de uma propriedade.
     * <p>
     * 
     * @param key
     *            nome da propriedade.
     * @return <code>true</code> se existir; do contrário <code>false</code>.
     */
    public boolean hasProperty(String key) {
        boolean exists = false;
        int i = 0;
        while ((!exists) && (i < properties.length)) {
            if (properties[i] != null) {
                exists = properties[i].containsKey(key);
            }
            i++;
        }
        return exists;
    }

    /**
     * Retorna todos os nomes das propriedades do arquivo.
     * <p>
     * 
     * @return Todos os nomes das propriedades do arquivo.
     */
    public Collection<String> getPropertyNames() {

        Collection<String> names = null;

        Map<String, String> localCache = new HashMap<String, String>();

        for (int i = 0; i < properties.length; i++) {

            Enumeration<Object> en = properties[i].keys();
            while (en.hasMoreElements()) {
                String name = (String) en.nextElement();
                if (!localCache.containsKey(name)) {
                    localCache.put(name, "value");
                }
            }

        }
        names = (Collection<String>) localCache.keySet();
        return names;
    }

    private Properties loadProperties(String filename) {
        try {
            Properties prop = new Properties();
            ClassLoader classLoader = this.getClass().getClassLoader();
            if (classLoader == null) {
                classLoader = ClassLoader.getSystemClassLoader();
            }

            /*
             * Loga o caminho completo do arquivo encontrado
             */
            URL url = classLoader.getResource(filename + ".properties");
            if (url != null) {
            	logger.info("ConfigProperties: carregando arquivo '" + url.getFile() + "'");
            }

            InputStream is = classLoader.getResourceAsStream(filename + ".properties");
            prop.load(is);
            is.close();
            return prop;
        }
        catch (Exception e) {
            return null;
        }
    }

    private String loadString(String propertyName) {
        for (int i = 0; i < properties.length; i++) {
            try {
                return properties[i].getProperty(propertyName).trim();
            }
            catch (Exception e) {
            }
        }
        return "";
    }
}