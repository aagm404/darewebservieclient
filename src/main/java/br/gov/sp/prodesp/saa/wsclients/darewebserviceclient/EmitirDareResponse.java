package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "emitirDareResult" })
@XmlRootElement(name = "EmitirDareResponse")
public class EmitirDareResponse {

	@XmlElement(name = "EmitirDareResult")
	protected DareServiceEmissaoResponseDTO emitirDareResult;

	public DareServiceEmissaoResponseDTO getEmitirDareResult() {
		return emitirDareResult;
	}

	public void setEmitirDareResult(DareServiceEmissaoResponseDTO value) {
		this.emitirDareResult = value;
	}
}