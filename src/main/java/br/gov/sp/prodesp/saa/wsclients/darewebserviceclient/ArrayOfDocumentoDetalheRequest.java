package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfDocumentoDetalheRequest", propOrder = { "documentoDetalheRequest" })
public class ArrayOfDocumentoDetalheRequest {

	@XmlElement(name = "DocumentoDetalheRequest", nillable = true)
	protected List<DocumentoDetalheRequest> documentoDetalheRequest;

	public List<DocumentoDetalheRequest> getDocumentoDetalheRequest() {
		if (documentoDetalheRequest == null) {
			documentoDetalheRequest = new ArrayList<DocumentoDetalheRequest>();
		}
		return this.documentoDetalheRequest;
	}
}