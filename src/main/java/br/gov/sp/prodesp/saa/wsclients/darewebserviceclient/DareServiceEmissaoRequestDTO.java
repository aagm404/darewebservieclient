package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DareServiceEmissaoRequestDTO", propOrder = { "documentoPrincipal", "documentosDetalhe" })
public class DareServiceEmissaoRequestDTO {

	@XmlElement(name = "DocumentoPrincipal")
	protected DocumentoPrincipalRequest documentoPrincipal;

	@XmlElement(name = "DocumentosDetalhe")
	protected ArrayOfDocumentoDetalheRequest documentosDetalhe;

	public DocumentoPrincipalRequest getDocumentoPrincipal() {
		return documentoPrincipal;
	}

	public void setDocumentoPrincipal(DocumentoPrincipalRequest value) {
		this.documentoPrincipal = value;
	}

	public ArrayOfDocumentoDetalheRequest getDocumentosDetalhe() {
		return documentosDetalhe;
	}

	public void setDocumentosDetalhe(ArrayOfDocumentoDetalheRequest value) {
		this.documentosDetalhe = value;
	}
}