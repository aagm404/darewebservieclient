package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

public class DareProperty {
	
	private static ConfigProperties properties = new ConfigProperties("dare");
	
	private static final String DEFAULT_MESSAGE 			  = "Chave nao encontrada no arquivo de propriedades do Sistema: ";
	private static final String DARE_URL   					  = "dare.url";
	private static final String DARE_LOGIN 					  = "dare.login";
	private static final String DARE_SENHA 					  = "dare.senha";
	private static final String DARE_CODIGO_RECEITA_MULTA 	  = "dare.codigo.receita.multa";
	private static final String DARE_CODIGO_RECEITA_TAXAS     = "dare.codigo.receita.taxas";
	private static final String DARE_CODIGO_ORGAO   		  = "dare.codigo.orgao";
	private static final String DARE_CODIGO_ENVIO   		  = "dare.codigo.envio";
	private static final String DARE_IDENTIFICADOR_REGISTRO   = "dare.identificador.registro";
	private static final String DARE_IDENTIFICADOR_EMISSOR    = "dare.identificador.emissor";
	private static final String DARE_CODIGO_FILA_SAIDA	      = "dare.codigo.fila.saida";
	
	private static String getStringProperty(String propertyName) {
        String value = properties.readString(propertyName);
        
        if (value == null || value.isEmpty()) {
            throw new RuntimeException(DEFAULT_MESSAGE + propertyName);
        }
        
        return value;
    }
	
	public static String getUrl() {
        return getStringProperty(DARE_URL);
    }
	
	public static String getLogin() {
		return getStringProperty(DARE_LOGIN);
	}

	public static String getSenha() {
		return getStringProperty(DARE_SENHA);
	}

	public static String getCodigoReceitaMulta() {
		return getStringProperty(DARE_CODIGO_RECEITA_MULTA);
	}

	public static String getCodigoReceitaTaxas() {
		return getStringProperty(DARE_CODIGO_RECEITA_TAXAS);
	}

	public static String getCodigoOrgao() {
		return getStringProperty(DARE_CODIGO_ORGAO);
	}
	
	public static String getCodigoEnvio() {
		return getStringProperty(DARE_CODIGO_ENVIO);
	}
	
	public static String getIdentificadorRegistro() {
		return getStringProperty(DARE_IDENTIFICADOR_REGISTRO);
	}

	public static String getIdentificadorEmissor() {
		return getStringProperty(DARE_IDENTIFICADOR_EMISSOR);
	}

	public static String getCodigoFilaSaida() {
		return getStringProperty(DARE_CODIGO_FILA_SAIDA);
	}
}