package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Erro", propOrder = { "codigoErro", "descricaoErro" })
public class Erro {

	@XmlElement(name = "CodigoErro")
	protected int codigoErro;
	
	@XmlElement(name = "DescricaoErro")
	protected String descricaoErro;

	public int getCodigoErro() {
		return codigoErro;
	}

	public void setCodigoErro(int value) {
		this.codigoErro = value;
	}

	public String getDescricaoErro() {
		return descricaoErro;
	}

	public void setDescricaoErro(String value) {
		this.descricaoErro = value;
	}
}