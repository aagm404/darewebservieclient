package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfErro", propOrder = { "erro" })
public class ArrayOfErro {

	@XmlElement(name = "Erro", nillable = true)
	protected List<Erro> erro;

	public List<Erro> getErro() {
		if (erro == null) {
			erro = new ArrayList<Erro>();
		}
		return this.erro;
	}
}