package br.gov.sp.prodesp.saa.wsclients.darewebserviceclient;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    private final static QName _AuthenticationSoapHeaderEntity_QNAME = new QName("http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", "AuthenticationSoapHeaderEntity");

    public ObjectFactory() {
    }

    public ConsultarDare createConsultarDare() {
        return new ConsultarDare();
    }

    public DareServiceConsultaRequestDTO createDareServiceConsultaRequestDTO() {
        return new DareServiceConsultaRequestDTO();
    }

    public ConsultarDareResponse createConsultarDareResponse() {
        return new ConsultarDareResponse();
    }

    public DareServiceConsultaResponseDTO createDareServiceConsultaResponseDTO() {
        return new DareServiceConsultaResponseDTO();
    }

    public AuthenticationSoapHeaderEntity createAuthenticationSoapHeaderEntity() {
        return new AuthenticationSoapHeaderEntity();
    }

    public EmitirDare createEmitirDare() {
        return new EmitirDare();
    }

    public DareServiceEmissaoRequestDTO createDareServiceEmissaoRequestDTO() {
        return new DareServiceEmissaoRequestDTO();
    }

    public EmitirDareResponse createEmitirDareResponse() {
        return new EmitirDareResponse();
    }

    public DareServiceEmissaoResponseDTO createDareServiceEmissaoResponseDTO() {
        return new DareServiceEmissaoResponseDTO();
    }

    public ArrayOfDocumentoDetalheRequest createArrayOfDocumentoDetalheRequest() {
        return new ArrayOfDocumentoDetalheRequest();
    }

    public DocumentoDetalheResponse createDocumentoDetalheResponse() {
        return new DocumentoDetalheResponse();
    }

    public DocumentoDetalheRequest createDocumentoDetalheRequest() {
        return new DocumentoDetalheRequest();
    }

    public ArrayOfErro createArrayOfErro() {
        return new ArrayOfErro();
    }

    public ArrayOfDocumentoDetalheResponse createArrayOfDocumentoDetalheResponse() {
        return new ArrayOfDocumentoDetalheResponse();
    }

    public Erro createErro() {
        return new Erro();
    }

    public DocumentoPrincipalRequest createDocumentoPrincipalRequest() {
        return new DocumentoPrincipalRequest();
    }

    public DocumentoPrincipalResponse createDocumentoPrincipalResponse() {
        return new DocumentoPrincipalResponse();
    }

    @XmlElementDecl(namespace = "http://schemas.pagamentos.fazenda.sp.gov.br/Pagamentos/WS/DareService", name = "AuthenticationSoapHeaderEntity")
    public JAXBElement<AuthenticationSoapHeaderEntity> createAuthenticationSoapHeaderEntity(AuthenticationSoapHeaderEntity value) {
        return new JAXBElement<AuthenticationSoapHeaderEntity>(_AuthenticationSoapHeaderEntity_QNAME, AuthenticationSoapHeaderEntity.class, null, value);
    }
}